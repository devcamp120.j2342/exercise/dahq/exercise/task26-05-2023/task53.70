package model;

public class Time {
    private int hour;
    private int minute;
    private int second;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public Time() {
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    @Override
    public String toString() {
        if (this.hour < 0 || this.hour > 24) {
            System.out.println("Nhap sai gio");
        }
        if (this.minute < 0 || this.minute > 60) {
            System.out.println("Nhap sai phut");
        }
        if (this.second < 0 || this.second > 60) {
            System.out.println("Nhap sai giay");
        }
        if (this.hour < 10 && this.minute < 10 && this.second < 10) {
            return "0" + this.hour + ":" + "0" + this.minute + ":" + "0" + this.second;

        }
        if (this.hour < 10 && this.minute < 10) {
            return "0" + this.hour + ":" + "0" + this.minute + ":" + this.second;
        }
        if (this.hour < 10 && this.second < 10) {
            return "0" + this.hour + ":" + this.minute + ":" + "0" + this.second;
        }
        if (this.second < 10 && this.minute < 10) {
            return this.hour + ":" + "0" + this.minute + ":" + "0" + this.second;
        }
        if (this.hour < 10) {
            return "0" + this.hour + ":" + this.minute + ":" + this.second;
        }
        if (this.minute < 10) {
            return this.hour + ":" + "0" + this.minute + ":" + this.second;
        }
        if (this.second < 10) {
            return this.hour + ":" + this.minute + ":" + "0" + this.second;
        }
        return this.hour + ":" + this.minute + ":" + this.second;

    }

    public Time nextSecond() {
        if (this.second == 59) {
            if (this.minute == 59) {
                if (this.hour > 24) {
                    this.hour = 1;
                    this.minute = 0;
                    this.second = 0;
                } else {
                    this.hour += 1;
                    this.minute = 0;
                    this.second = 0;
                }
            } else {
                this.minute += 1;
                this.second = 0;
            }
        } else {
            this.second += 1;
        }
        return new Time(this.hour, this.minute, this.second);
    }

    public Time previousSecond() {
        if (this.second == 0) {
            if (this.minute == 0) {
                if (this.hour == 0) {
                    this.hour = 24;
                    this.minute = 59;
                    this.second = 59;
                } else {
                    this.hour -= 1;
                    this.minute = 59;
                    this.second = 59;
                }
            } else {
                this.minute -= 1;
                this.second = 59;
            }
        } else {
            this.second -= 1;
        }
        return new Time(this.hour, this.minute, this.second);
    }

}
